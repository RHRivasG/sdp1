using System;
using System.Net;
using System.Text;

namespace DistributedCore.Server {
    public class ReadingException : Exception {
        public IPEndPoint BindedAddress { get; private set; }
        public byte[] ReceivedMessage { get; private set; }

        public ReadingException(
            byte[] message,
            IPEndPoint binded,
            Exception inner
        ): base(inner.Message) {
            ReceivedMessage = message;
            BindedAddress = binded;
        }

        public override string Message {
            get {
                var message = Encoding.UTF8.GetString(ReceivedMessage);
                return
                    $@"Occurred error reading from {BindedAddress},
                    read: {message}, exception: {base.Message}";
            }
        }
    }
}
