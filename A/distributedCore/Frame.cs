using System;
using System.Net;
using System.Threading.Tasks;
using DistributedCore.Server;
using Newtonsoft.Json;

namespace DistributedCore.Message {
    public class Frame
    {
        [JsonConverter(typeof(IPEndPointConverter))]
        [JsonProperty("destino")]
        public virtual IPEndPoint Destination { get; set; }
        [JsonProperty("consumir")]
        public virtual int AmountToBuy { get; set; }

        public virtual Task<int> Run(IMonitorA monitor) {
            Console.WriteLine($"[{DateTime.Now}] Llamando primitiva llenar");
            return monitor.Consumir(AmountToBuy);
        }
    }

    public class FillFrame: Frame {
        public async override Task<int> Run(IMonitorA monitor) {
            Console.WriteLine($"[{DateTime.Now}] Llamando primitiva llenar");
            await monitor.Llenar();
            return -1;
        }
    }
}
