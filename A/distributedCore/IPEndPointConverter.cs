using System;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DistributedCore.Message {
    public class IPEndPointConverter : JsonConverter
    {
        public override bool CanConvert(Type typeToConvert)
            => typeof(IPEndPoint) == typeToConvert;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType == typeof(IPEndPoint)) {
                return IPEndPoint.Parse(serializer.Deserialize<string>(reader));
            }

            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is IPEndPoint ep) {
                writer.WriteValue(value.ToString());
            }
        }
    }
}
