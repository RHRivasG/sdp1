using System.Threading.Tasks;

namespace DistributedCore.Server {
    public interface IMonitorA {
        Task Llenar();
        Task<int> Consumir(int n);
    }
}
