﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DistributedCore.Message;

namespace DistributedCore.Server
{
    public class Server<T>
    {
        public IObservable<Request<T>> Packages { get; }

        public Server(Func<UdpReceiveResult, T> fact, int port) {
            var host = Dns.GetHostName();

            var address = IPAddress.Any;

            Packages =
                Observable.Create<Request<T>>(async (o) => {
                    var localEp = new IPEndPoint(address, port);
                    UdpClient server = new UdpClient(localEp);
                    UdpReceiveResult message = default;
                    while (true) {
                        try {
                            message  = await server.ReceiveAsync();
                            var frame = fact(message);
                            o.OnNext(
                                new Request<T>() {
                                    Server = server,
                                        Sender = message.RemoteEndPoint,
                                        Receiver = localEp,
                                        Message = frame
                                        }
                            );
                        } catch(ObjectDisposedException) {
                            o.OnCompleted();
                            break;
                        } catch(Exception e) {
                            o.OnError(
                                new ReadingException(
                                    message.Buffer,
                                    localEp,
                                    e
                                )
                            );
                            break;
                        }
                    }
                    return server;
                })
                .Publish()
                .RefCount()
                .ObserveOn(TaskPoolScheduler.Default);
        }
    }
}
