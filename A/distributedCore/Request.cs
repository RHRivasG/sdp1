using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCore.Message {
    public class Request<T> {

        public UdpClient Server { private get; init; }

        public IPEndPoint Sender { get; init; }

        public IPEndPoint Receiver { get; init; }

        public T Message { get; init; }

        public Task<int> RespondAsync(string message) {
            var mBytes = Encoding.UTF8.GetBytes(message);
            return Server.SendAsync(mBytes, mBytes.Length, Sender);
        }

        public int Respond(string message) {
            var mBytes = Encoding.UTF8.GetBytes(message);
            return Server.Send(mBytes, mBytes.Length, Sender);
        }

    }
}
