﻿using System;
using System.Net.Sockets;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using DistributedCore.Server;
using Newtonsoft.Json;

namespace DistributedProducer
{
    class Program
    {
        async static Task Main(string[] args)
        {
            var random = new Random();

            var socket = new UdpClient();

            socket.Connect("192.168.1.7", 5436);

            var server  = new Server<string>(
                msg => Encoding.UTF8.GetString(msg.Buffer),
                5437
            );

            await server
                .Packages
                .Do(req => Console.WriteLine(
                        $"[{DateTime.Now}] Recibido: {JsonConvert.SerializeObject(req.Message)}"
                    )
                )
                .Where(req => req.Message is "notificar")
                .ForEachAsync(async req => {
                    var bytes = Encoding.UTF8.GetBytes("llenar");
                    Console.WriteLine($"[{DateTime.Now}] Enviando trama 'llenar' a la tienda");
                    await socket.SendAsync(bytes, bytes.Length);
                });
        }
    }
}
