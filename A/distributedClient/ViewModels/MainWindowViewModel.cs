﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DistributedClient.Models;
using DistributedClient.Services;
using DynamicData;
using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using System.Threading.Tasks;

namespace DistributedClient.ViewModels
{
    using StoreList = ReadOnlyObservableCollection<StoreConfiguration>;

    public class MainWindowViewModel : ViewModelBase, IActivatableViewModel,
        IValidatableViewModel
    {
        StoreList _list = new StoreList(new());

        StoreConfiguration selectedStore;

        readonly ObservableAsPropertyHelper<int> consumed;

        public int Consumed => consumed.Value;

        public StoreList StoreList => _list;

        public StoreConfiguration StoreMux { get; } = StoreConfiguration.Probe;

        public StoreConfiguration SelectedStore {
            get => selectedStore;
            set => this.RaiseAndSetIfChanged(ref selectedStore, value);
        }

        public ReactiveCommand<Unit, Unit> Configure { get; private set; }

        public ReactiveCommand<Unit, Unit> SelectStore { get; private set; }

        public ReactiveCommand<Unit, int> ConsumeStore { get; private set; }

        public ViewModelActivator Activator { get; }

        public ValidationContext ValidationContext => new();

        public MainWindowViewModel(IStoreService service) {
            Activator = new();
            this.WhenActivated(disposer => {
                var hostNotEmpty =
                    StoreMux
                    .WhenAnyValue(
                        st => st.Host,
                        host => !string.IsNullOrEmpty(host)
                    );

                this.ValidationRule(
                    vm => vm.StoreMux,
                    hostNotEmpty,
                    "El host no puede estar vacio"
                ).DisposeWith(disposer);
            });

            service
                .Connect()
                .ObserveOn(RxApp.MainThreadScheduler)
                .Bind(out _list)
                .Subscribe();

            var canConfigure =
                StoreMux.WhenAnyValue(vm => vm.Valid).Select(x => x);

            var canSelectStore =
                this
                .WhenAnyValue(
                    x => x.SelectedStore,
                    x => x.StoreList.Count,
                    (st, c) => c is not 0 || st is not null
                );

            var consumed = 0;

            var canConsumeStore =
                this
                .WhenAnyValue(
                    vm => vm.SelectedStore,
                    vm => vm.SelectedStore.ConsumeAmount,
                    (_, x) => Int32.Parse(x)
                )
                .Select(t => t is > 0);

            ConsumeStore =
                ReactiveCommand
                .CreateFromTask(
                    async () => {
                        return
                            consumed +=
                            await service.ConsumeFromStore(StoreMux, SelectedStore);
                    },
                    canConsumeStore
                );

            ConsumeStore.ToProperty(this, vm => vm.Consumed, out this.consumed);

            Configure = ReactiveCommand.CreateFromTask(
                async () => await service.ProbeMux(StoreMux)
                , canConfigure
            );

            SelectStore = ReactiveCommand
                .CreateFromTask(
                    async () => {
                        await DialogHost.DialogHost.Show(this, "Stores");
                    },
                    canSelectStore
                );
        }
    }
}
