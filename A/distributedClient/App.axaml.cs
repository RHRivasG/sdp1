using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using DistributedClient.Services;
using DistributedClient.ViewModels;
using DistributedClient.Views;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ReactiveUI;
using Splat;
using Splat.Microsoft.Extensions.DependencyInjection;

namespace DistributedClient
{
    public class App : Application
    {
        public IServiceProvider? Container { get; private set; }

        public App() {
            // Init();
        }

        // void Init() {
        //     var host =
        //         Host
        //         .CreateDefaultBuilder()
        //         .ConfigureServices(services => {
        //             services.UseMicrosoftDependencyResolver();
        //             var resolver = Locator.CurrentMutable;
        //             resolver.InitializeSplat();
        //             resolver.InitializeReactiveUI();
        //
        //             ConfigureServices(services);
        //         })
        //         .UseEnvironment(Environments.Development)
        //         .Build();
        //
        //     Container = host.Services;
        //     Container.UseMicrosoftDependencyResolver();
        // }

        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            Locator.CurrentMutable.RegisterLazySingleton<IStoreService>(
                () => new RemoteStoreService()
            );

            if (ApplicationLifetime
                is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainWindow {
                    ViewModel = new MainWindowViewModel(
                        Locator.Current.GetService<IStoreService>()
                    )
                };
            }

            base.OnFrameworkInitializationCompleted();
        }

        // void ConfigureServices(IServiceCollection services) {
        //     services.AddSingleton<IViewLocator, AppViewLocator>();
        //     services.AddSingleton<IStoreService, MockStoreService>();
        // }
    }
}
