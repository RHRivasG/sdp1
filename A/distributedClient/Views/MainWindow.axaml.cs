using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using DistributedClient.ViewModels;
using Newtonsoft.Json;
using ReactiveUI;

namespace DistributedClient.Views
{
    public partial class MainWindow : ReactiveWindow<MainWindowViewModel>
    {
        Button Configure => this.FindControl<Button>("Configure");
        Button Consume => this.FindControl<Button>("Consume");
        TextBlock Consumed => this.FindControl<TextBlock>("Consumed");
        TextBox HostInput => this.FindControl<TextBox>("HostInput");
        TextBox PortInput => this.FindControl<TextBox>("PortInput");
        DialogHost.DialogHost Stores => this.FindControl<DialogHost.DialogHost>("Stores");

        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
            this.WhenActivated(disposer => {
                this.Bind(
                    ViewModel,
                    vm => vm.StoreMux.Host,
                    wnd => wnd.HostInput.Text
                ).DisposeWith(disposer);

                this.Bind(
                    ViewModel,
                    vm => vm.StoreMux.Port,
                    wnd => wnd.PortInput.Text
                ).DisposeWith(disposer);

                this.BindCommand(
                    ViewModel,
                    vm => vm.ConsumeStore,
                    wnd => wnd.Consume
                );

                this.OneWayBind(
                    ViewModel,
                    vm => vm.Consumed,
                    wnd => wnd.Consumed.Text
                );

                ViewModel
                    .WhenAnyObservable(
                        vm => vm.SelectStore.CanExecute,
                        vm => vm.Configure.IsExecuting,
                        (ce, ie) => new {
                            CanExecute = ce,
                            IsExecuting = ie
                        }
                    )
                    .Subscribe(info => {
                        if (info.CanExecute
                            && !info.IsExecuting
                            && !Configure.Classes.Contains("Show")
                        ) {
                            Configure.Classes.Add("Show");
                            Configure.Classes.Remove("Hidden");
                        }
                        else if (
                            !DialogHost.DialogHost.IsDialogOpen("Stores") &&
                            !info.IsExecuting &&
                            !info.CanExecute
                        )
                        {
                            Configure.Classes.Remove("Show");
                            Configure.Classes.Add("Hidden");
                        }
                    })
                    .DisposeWith(disposer);
            });
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
