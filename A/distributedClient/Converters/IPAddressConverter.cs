using System;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReactiveUI;

namespace DistributedClient.Converters {

    public class IPAddressConverter : JsonConverter, IBindingTypeConverter
    {
        public override bool CanConvert(Type objectType)
            => typeof(IPAddress) == objectType;

        public int GetAffinityForObjects(Type fromType, Type toType)
        {
            return fromType == typeof(String) ? 1 : 0;
        }

        public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
        {
            if (objectType == typeof(IPAddress))
                return IPAddress.Parse(serializer.Deserialize<string>(reader));
            return null;
        }

        public bool TryConvert(object? from,
                               Type toType,
                               object? conversionHint,
                               out object? result)
        {
            try {
                result = IPAddress.Parse(from.ToString());
                return true;
            }
            catch {
                result = null;
                return false;
            }
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {
            if (value is IPAddress addr)
                JToken.FromObject(addr.ToString()).WriteTo(writer);
        }
    }
}
