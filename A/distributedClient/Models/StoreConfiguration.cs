using System;
using System.Net;
using System.Reactive.Linq;
using DistributedCore.Message;
using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Validation.Contexts;

namespace DistributedClient.Models {

    public class StoreConfiguration: ReactiveObject {
        long port;
        string host = "";
        int consumeAmount;
        readonly ObservableAsPropertyHelper<bool> valid;
        readonly ObservableAsPropertyHelper<IPEndPoint> server;

        public bool Valid => valid.Value;

        [JsonProperty("port")]
        public string Port {
            get => port.ToString();
            set {
                var valid = Int64.TryParse(value, out long lValue);
                if (valid)
                    this.RaiseAndSetIfChanged(ref port, Math.Min(lValue, 99999));
            }
        }

        [JsonProperty("host")]
        public string Host {
            get => host;
            set => this.RaiseAndSetIfChanged(ref host, value);
        }

        public string ConsumeAmount {
            get => consumeAmount.ToString();
            set {
                var valid = Int32.TryParse(value, out int lValue);
                if (valid)
                    this.RaiseAndSetIfChanged(ref consumeAmount, lValue);
            }
        }

        public IPEndPoint Server => server.Value;

        public ValidationContext ValidationContext => new();

        IPEndPoint? BuildIPE(long port, string host) {
            try {
                return new IPEndPoint(
                    IPAddress.Parse(host),
                    (int) Math.Min((uint) IPEndPoint.MaxPort, (uint) port)
                );
            } catch {
                return null;
            }
        }

        bool Validate(long port, string host)
        {
            return !string.IsNullOrEmpty(host) &&
                IPAddress.TryParse(host, out IPAddress? addr) &&
                port is <= IPEndPoint.MaxPort and >= IPEndPoint.MinPort and not 0
                ;
        }

        [JsonConstructor]
        public StoreConfiguration() {
            this
                .WhenAnyValue(vm => vm.Host, vm => vm.Port)
                .Select(t => (t.Item1, Int64.Parse(t.Item2)))
                .Where(t => Validate(t.Item2, t.Item1))
                .Select(t => BuildIPE(t.Item2, t.Item1))
                .Where(t => t is not null)
                .ToProperty(this, vm => vm.Server, out server);

            this
                .WhenAnyValue(vm => vm.Host, vm => vm.Port)
                .Select(t => (t.Item1, Int64.Parse(t.Item2)))
                .Select(t => Validate(t.Item2, t.Item1))
                .ToProperty(this, vm => vm.Valid, out valid);
        }

        public static explicit operator Frame(StoreConfiguration conf)
        {
            return new Frame() {
                Destination = conf.Server,
                AmountToBuy = conf.consumeAmount
            };
        }

        public static StoreConfiguration Probe = new StoreConfiguration() {
            ConsumeAmount = "-1"
        };
    }

}
