using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using DistributedClient.Models;
using DistributedCore.Message;
using DynamicData;
using Newtonsoft.Json;

namespace DistributedClient.Services {

    public class RemoteStoreService : IStoreService
    {
        private readonly SourceList<StoreConfiguration > _source = new();

        public IObservable<IChangeSet<StoreConfiguration >> Connect() =>
            _source.Connect();

        public async Task<int> ConsumeFromStore(StoreConfiguration dest, StoreConfiguration conf)
        {
            using var storeConnection = new UdpClient();
            try {
                var message =
                        JsonConvert.SerializeObject((Frame)conf);
                var messageBytes =
                    Encoding.UTF8.GetBytes(
                        message
                    );

                Console.WriteLine($"[{DateTime.Now}] Enviando a {dest.Server} trama: {message}");

                await storeConnection.SendAsync(
                    messageBytes,
                    messageBytes.Length,
                    dest.Server
                );

                var storeResponse = await await
                    Task.WhenAny(
                        storeConnection.ReceiveAsync(),
                        Task.Delay(5000).ContinueWith(_ => new UdpReceiveResult())
                    );

                var msg = Encoding.UTF8.GetString(storeResponse.Buffer);

                Console.WriteLine($"[{DateTime.Now}] Recibido {msg} de: {storeResponse.RemoteEndPoint}");

                return JsonConvert.DeserializeObject<int>(
                    Encoding.UTF8.GetString(storeResponse.Buffer)
                );
            }
            catch(Exception e) {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return 0;
            }
        }

        public async Task ProbeMux(StoreConfiguration conf)
        {
            using var storeConnection = new UdpClient();

            try {
                var message = JsonConvert.SerializeObject((Frame)conf);
                var messageBytes = Encoding.UTF8.GetBytes(message);

                Console.WriteLine($"[{DateTime.Now}] Enviando a {conf.Server} trama: {message}");

                await storeConnection.SendAsync(
                    messageBytes,
                    messageBytes.Length,
                    conf.Server
                );

                storeConnection.Connect(conf.Server);

                var hostsResponse = await await
                    Task.WhenAny(
                        storeConnection.ReceiveAsync(),
                        Task.Delay(3000).ContinueWith(
                            _ => new UdpReceiveResult()
                        )
                    );

                var msg = Encoding.UTF8.GetString(hostsResponse.Buffer);

                var hosts =
                    JsonConvert
                    .DeserializeObject<StoreConfiguration[]>(
                        Encoding.UTF8.GetString(hostsResponse.Buffer)
                    );

                Console.WriteLine($"[{DateTime.Now}] Recibido {msg} de: {hostsResponse.RemoteEndPoint}");

                _source.Clear();

                _source.AddRange(hosts);

            } catch(Exception e) {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

        }
    }

}
