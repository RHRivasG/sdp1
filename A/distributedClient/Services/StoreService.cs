using System;
using System.Threading.Tasks;
using DistributedClient.Models;
using DynamicData;

namespace DistributedClient.Services {
    public interface IStoreService {
        Task ProbeMux(StoreConfiguration conf);
        Task<int> ConsumeFromStore(StoreConfiguration dest, StoreConfiguration conf);
        IObservable<IChangeSet<StoreConfiguration>> Connect();
    }
}
