using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using DistributedCore.Server;
using Store.Synchronization;

namespace Store.Service {
    // <summary>
    // Servicio implementador de la tienda, mantiene la cantidad de
    // productos de la tienda e implementa sus metodos como monitor
    // </summary>
    public class StoreService: IMonitorA, IDisposable {
        int products; // Cantidad de productos actuales
        IPEndPoint producerEp; // Punto de entrada del productor
        ConditionVariableSlim cv; // Variable de condicion del monitor
        UdpClient producer; // Unico socket de comunicacion con el productor

        public StoreService(IPEndPoint producerEp = null) {
            this.producerEp =  producerEp ?? IPEndPoint.Parse("127.0.0.1:5437");

            producer = new UdpClient();
            cv = new ConditionVariableSlim();
        }

        public async Task Llenar() {
            await cv.Inner.WaitAsync(); // Bloquea el metodo
            products += 5; // Aumenta la cantidad de productos
            cv.Release(); // Libera la variable de condicion
            cv.Inner.Release(); // Libera el metodo
        }

        public async Task<int> Consumir(int cantidad) {
            await cv.Inner.WaitAsync(); // Bloquea el metodo
            while (cantidad > products) {
                Console.WriteLine("Sin productos suficientes");
                var messageBytes = Encoding.UTF8.GetBytes("notificar");
                await producer.SendAsync( // Envia el mensaje al productor
                    messageBytes,
                    messageBytes.Length,
                    producerEp
                );
                Console.WriteLine("Esperando a llenado");
                await cv.WaitAsync(); // Libera el metodo y espera a ser notificado
                Console.WriteLine($"Llenado completo: {products}");
            } // Una vez notificado, vuelve a bloquear el metodo y si hay suficientes productos sale
            products -= cantidad; // Consume productos
            cv.Inner.Release(); // Libera el metodo
            return cantidad;
        }

        public void Dispose() => producer.Dispose();
    }
}
