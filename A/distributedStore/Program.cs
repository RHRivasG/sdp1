﻿using System;
using System.Net;
using System.Reactive.Linq;
using DistributedCore.Server;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using DistributedCore.Message;
using System.Linq;
using Store.Service;

namespace Store
{
    class Program
    {
        async static Task Main(string[] args) {
            var service = new StoreService();

            var addresses =
                (await Dns.GetHostEntryAsync(Dns.GetHostName()))
                .AddressList
                .Skip(1); // Obtener todas las direcciones disponibles

            var server = new Server<Frame>(
                msg => {
                    var msgString = Encoding.UTF8.GetString(msg.Buffer);
                    if (msgString != "llenar")
                        return JsonConvert.DeserializeObject<Frame>(msgString);
                    else
                        return new FillFrame();
                },
                5436
            ); // Iniciar la implementacion del servidor

            var validPackages =
                server
                .Packages
                .Where(req =>
                       (
                           req.Message.Destination is not null &&
                           addresses.Contains(req.Message.Destination.Address)
                       ) ||
                       (
                           req.Message.AmountToBuy is 0 &&
                           req.Message.Destination is null
                       )
                ); // Aceptar solo paquetes dirigidos a esta maquina y de llenado

            var forwarder =
                validPackages
                .Where(req => req.Message.AmountToBuy is -1)
                .Do(req =>
                    Console.WriteLine(
                        $"[{DateTime.Now}] Recibido: {JsonConvert.SerializeObject(req.Message)}"
                    )
                )
                .Do(async req => {
                    var addr = req.Message.Destination.Address;
                    var response = $"[{{host:\"{addr}\",port:5436}}]";
                    Console.WriteLine($"Respondiendo con {response}");
                    await req.RespondAsync(response);
                })
                .Subscribe(); // Si el paquete es de sondeo, enviar la respuesta
                              // de la informacion del host con la direccion
                              // a la que fue enviada el paquete

            await validPackages
                .Where(req => req.Message.AmountToBuy is not -1)
                .Do(req =>
                    Console.WriteLine(
                        $"[{DateTime.Now}] Recibido: {JsonConvert.SerializeObject(req.Message)}"
                    )
                )
                .Finally(delegate { forwarder.Dispose(); service.Dispose(); }) // Destruye la tienda y el manejo de
                                                                               // paquetes de sondeo cuando termina de procesar
                                                                               // paquetes
                .ForEachAsync(
                    async req => {
                        var n = await req.Message.Run(service); //
                        if (n > 0)
                            await req.RespondAsync(n.ToString());
                    }
                ); // Corremos el metodo del monitor correspondiente al mensaje:
                   // Frame => Consumir y se envia una respuesta de vuelta
                   // FillFrame => Llenar
        }
    }
}
