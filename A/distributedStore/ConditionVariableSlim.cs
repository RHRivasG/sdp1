using System.Threading;
using System.Threading.Tasks;

namespace Store.Synchronization {
    // <summary>
    // Implementacion simple de una variable de condicion a partir de dos
    // semaforos
    // </summary>
    public class ConditionVariableSlim: SemaphoreSlim {
        public SemaphoreSlim Inner => new SemaphoreSlim(1);

        public ConditionVariableSlim(): base(0, 1) {}

        // <summary>
        // Sobrescribe la operacion de espera para liberar el semaforo interno
        // y esperar a ser notificado
        // </summary>
        public async new Task WaitAsync() {
            Inner.Release();
            await base.WaitAsync();
            await Inner.WaitAsync();
        }
    }
}
