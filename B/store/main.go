package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

type SafeCounter struct {
	Value int
	Mu    sync.Mutex
}

type Pedido struct {
	Gastar  int
	Destino string
}

//"cond" variable de condicion asociada al mutex "Mu"
//de SafeCounter para la contabilidad de productos
var productos SafeCounter
var cond = sync.NewCond(&productos.Mu)

func gastar(n int) {
	productos.Value -= n
}

func llenar() {
	productos.Value += 5
}

func main() {

	//Contruccion de direccion udp para la comunicacion de
	//peticiones del cliente y respuestas del productor
	udpAddr, err := net.ResolveUDPAddr("udp", "0.0.0.0:2222")
	if err != nil {
		fmt.Println("Wrong Address")
		return
	}

	udpConn, err := net.ListenUDP("udp", udpAddr)

	file, err := udpConn.File()

	syscall.SetsockoptInt(int(file.Fd()), syscall.IPPROTO_IP, syscall.IP_PKTINFO, 1)

	data := make([]byte, 1024)
	oob := make([]byte, 1024)

	fmt.Println("Tienda B funcionando")

	for {

		n, _, _, addr, err := udpConn.ReadMsgUDP(data, oob)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(string(data[:n]))
		oob_buffer := bytes.NewBuffer(oob)

		msg := syscall.Cmsghdr{}
		binary.Read(oob_buffer, binary.LittleEndian, &msg)

		host := ""
		if msg.Level == syscall.IPPROTO_IP && msg.Type == syscall.IP_PKTINFO {
			packet_info := syscall.Inet4Pktinfo{}
			binary.Read(oob_buffer, binary.LittleEndian, &packet_info)

			for _, nu := range packet_info.Addr {
				host += strconv.Itoa(int(nu)) + "."
			}
			host = strings.TrimSuffix(host, ".")

		}

		//Llamada concurrente de funcion para cada
		//peticion entrante por el puerto 2222
		go display(udpConn, data[0:n], addr, host)
	}

}

func display(conn *net.UDPConn, buf []byte, addr *net.UDPAddr, host string) {

	var p Pedido

	//Bloqueo de variable de condicion para encolar las peticiones
	//simultaneas de las conexiones en el puerto 2222.
	cond.L.Lock()

	if string(buf) == "llenar" {

		llenar()

		//Terminada la peticion del productor se continua con
		//la peticion del cliente
		cond.Signal()

	} else {
		if err := json.Unmarshal(buf, &p); err != nil {
			fmt.Println(err)
			return
		}

		if p.Gastar != -2 {

			fmt.Println("Peticion: Gastar ", p.Gastar, " productos de la tienda")

			for productos.Value < p.Gastar {
				fmt.Println("No suficientes productos en la tienda")
				connProducer()
			}

			gastar(p.Gastar)

			//Devuelve al cliente cantidad de productos entregados
			if _, err := conn.WriteToUDP([]byte(strconv.Itoa(p.Gastar)), addr); err != nil {
				log.Fatal(err)
			}

			fmt.Println("Gastos: ", p.Gastar, " productos gastados de la tienda")
			fmt.Println("Pedido por: ", addr)
			fmt.Println("Desde: ", host, ":2222")
			fmt.Println("Hora: ", time.Now())

		} else {

			res := `[{"host": "` + host + `","port":2222}]`

			if _, err := conn.WriteToUDP([]byte(res), addr); err != nil {
				log.Fatal(err)
			}

		}
	}

	fmt.Println("Total de productos en la tienda: ", productos.Value)

	cond.L.Unlock()
}

func connProducer() {

	dest, err := net.ResolveUDPAddr("udp", "192.168.122.97:2223")

	//Conexion creada para notificar al productor
	udpProd, err := net.ResolveUDPAddr("udp", ":2224")
	if err != nil {
		log.Fatal(err)
	}

	udpConn, err := net.ListenUDP("udp", udpProd)
	if err != nil {
		log.Fatal(err)
	}

	if _, err = udpConn.WriteToUDP([]byte("notificar"), dest); err != nil {
		log.Fatal(err)
	}

	//Variable de condicion en espera a que se termine las peticiones
	//con el productor antes de seguir con la peticion del cliente
	cond.Wait()

	if err = udpConn.Close(); err != nil {
		log.Fatal(err)
	}

}
