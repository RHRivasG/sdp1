package main

import (
	"fmt"
	"net"
	"strings"
)

func main() {

	//Contruccion de direccion udp para la comunicacion de
	//peticiones de la tienda
	udpAddr, err := net.ResolveUDPAddr("udp", ":2223")
	if err != nil {
		fmt.Println("Wrong Address")
		return
	}

	udpConn, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Productor B funcionando")

	for {
		var buf [2048]byte

		//Crear direccion para la conexion con la tienda
		addr, err := net.ResolveUDPAddr("udp", "192.168.122.1:2222")
		n, _, err := udpConn.ReadFromUDP(buf[0:])
		if err != nil {
			fmt.Println("Error Reading")
			return
		}

		//Llamada concurrente de funcion para cada
		//peticion entrante por el puerto 2223
		go display(buf[:n], addr, udpConn)
	}

}

func display(buf []byte, addr *net.UDPAddr, conn *net.UDPConn) {
	action := strings.TrimSuffix(string(buf), "\n")
	if strings.EqualFold(action, "notificar") {
		fmt.Println("Produciendo 5 productos para Tienda B")

		//Envio de productos invocando la funcion llenar
		//para la tienda
		_, err := conn.WriteToUDP([]byte("llenar"), addr)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}
