# frozen_string_literal: true
require 'json'
require 'logger'
require_relative 'message'
require 'socket'

module MiddlewareListener
  ##
  # Clase que representa un destino de un paquete
  # se usa para responder a un mensaje con destino conocido
  # y para devolver las direcciones conocidas en un sondeo
  class Destination
    attr_reader :port, :address
    attr_writer :method
    attr_accessor :logger

    def initialize(address, port, method = :to_z, &block)
      @address = address
      @port = port
      @method = block_given? ? proc(&block) : method
      @connected_stores = []
    end

    ##
    # Responde a un mensaje con direccion que este destino conoce
    def reply(responder, msg)
      socket = UDPSocket.open
      socket.connect(@address, @port)
      parsed_frame = JSON.generate(transform(msg))
      logger.info "Enviando a #{@address}:#{@port}: #{parsed_frame}"
      socket.send(parsed_frame, 0)
      msg, _ = socket.recvfrom(1024)
      responder.reply(msg)
      socket.close
    end

    ##
    # Registra una direccion que este destino conoce
    def register(address, port)
      @connected_stores << "#{address}:#{port}"
    end

    def hosts?(ep)
      @connected_stores.include?(ep)
    end

    def to_address
      return [{ host: address, port: port }] if @connected_stores.empty?

      return @connected_stores.map do |key|
        host, port = key.split(/:/)
        { host: host, port: port }
      end
    end

    private

    def transform(msg)
      return msg.public_send @method if @method.is_a? Symbol
      @method.call(msg)
    end
  end

  ##
  # Agregado de la clase Destino que implementa las funciones de busqueda
  # de destinos en funcion a Endpoints IPv4
  class Destinations
    def initialize
      @destinations = {}
    end

    def values
      @destinations.values
    end

    ##
    # Registrar nuevo destino bajo una direccion IPv4
    def []=(key, value)
      @destinations[key] = value
    end

    ##
    # Encuentra el destino registrado a esta direccion IPv4 o algun destino
    # que conozca esta direccion
    def [](key)
      _, dest = @destinations.find { |k, v| k === key || v.hosts?(key) }
      dest
    end
  end

  ##
  # Modulo de soporte para implementar el Unmarshalling de las tramas
  module Parser
    def parse(json)
      _, klass = mapper.find { |k, v| json.match(/\"#{k.to_s}\":/) }
      JSON.parse(json, { object_class: klass })
    end

    def when_key(key, parse:)
      mapper[key] = parse
    end

    private
    def mapper
      @mapper ||= {}
    end
  end


  ##
  # Clase que implementa el escuchado de mensajes en un socket UDP
  class Listener
    extend Parser

    ##
    # Corre el ciclo de escuchado de mensajes y redirecciona segun corresponda
    def run
      udp_server_socket = UDPSocket.new
      prepare_socket(udp_server_socket)
      local_addr = udp_server_socket.local_address
      loop do
        msg, source = udp_server_socket.recvfrom(1024)
        msg_src = Socket::UDPSource.new(source, local_addr) do |reply_msg|
          udp_server_socket.send(reply_msg, 0, source[3], source[1])
        end
        logger.info("Received message #{msg}")
        frame = self.class.parse(msg)
        destination = registered_nodes[frame.to_z.destino]
        if destination || frame.to_z.destino.empty? || frame.to_z.probe?
          Thread.new do
            unless frame.to_z.probe?
              destination.reply(msg_src, frame)
            else
              hosts = registered_nodes.values.flat_map(&:to_address)
              msg = JSON.generate hosts
              logger.info("Enviando respuesta: #{msg}")
              msg_src.reply(msg)
            end
          end
        else
          logger.warn(
            "Paquete #{msg} siendo ignorado porque no se reconoce el destino"
          )
        end
      end
    end

    ##
    # Registra una nueva direccion con el tipo de trama que escucha
    def add_destination(*args, &block)
      if (dest = args[0]).is_a? Destination
        dest.logger = logger
        yield dest
        @registered_nodes["#{dest.address}:#{dest.port}"] = dest
      else
        addr, port = args[0], args[1]
        addr_key = "#{addr}:#{port}"
        destination = Destination.new(addr, port, args[3], &block)
        destination.logger = logger
        @registered_nodes[addr_key] = destination
      end
    end

    protected
    attr_reader :address, :port, :registered_nodes, :logger

    ##
    # Vincula el socket a la direccion recibida
    def prepare_socket(socket)
      socket.bind(@address, @port)
    end

    def initialize(address, port = 5435)
      @address = address
      @port = port
      @registered_nodes = Destinations.new
      @logger = Logger.new(STDOUT)
      @logger.level = Logger::INFO
    end
  end
end
