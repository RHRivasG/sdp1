# frozen_string_literal: true

module Protocol
  ##
  # Clase que implementa la trama del protocolo Z
  class MessageZ
    attr_accessor :comprar, :destino

    def initialize(comprar: nil, destino: nil)
      self.comprar = comprar
      self.destino = destino
    end

    def []=(key, value)
      send "#{key}=", value
    end

    def to_z
      self
    end

    def to_json(*args)
      {
        comprar: self.comprar,
        destino: self.destino
      }.to_json(*args)
    end

    def probe?
      comprar < 0
    end
  end

  ##
  # Modulo de soporte que implementa el Marshalling y Unmarshalling
  # de tramas de un protocolo X a un protocolo Z
  module Marshall
    def of(obj)
      if obj.is_a? MessageZ
        map =
          mapper
            .reject { |k, v| k == :probe }
            .map { |k, v| [ v, obj.send(k) ] }
            .to_h
        result = self.new
        map.each do |k, v|
          result[k] = v unless v == -1
          result[k] = mapper[:probe] if v == -1
        end
        result
      end
    end

    def build(obj)
      if obj.is_a? self
        map =
          mapper
            .reject { |k, v| k == :probe }
            .map { |k, v| [ k, obj.send(v) ] }
            .to_h
        MessageZ.new(
          comprar: map[:comprar] == mapper[:probe] ? -1 : map[:comprar],
          destino: map[:destino]
        )
      end
    end

    def register(key_a, key_b = nil)
      self.attr_accessor key_a
      mapper[key_b || key_a] = key_a
    end

    def probe_value(val)
      mapper[:probe] = val
    end

    protected
    def mapper
      @mapper ||= {}
    end
  end

  class Frame
    extend Marshall

    def to_z
      self.class.build(self)
    end

    def []=(key, value)
      send "#{key}=", value
    end

    def is_probe
      to_z.comprar < 0
    end
  end

  ##
  # Clase que implementa la trama del protocolo A
  class MessageA < Frame
    register :destino
    register :consumir, :comprar
    probe_value -1

    def to_json(*args)
      {
        consumir: self.consumir,
        destino: self.destino
      }.to_json(*args)
    end
  end

  ##
  # Clase que implementa la trama del protocolo B
  class MessageB < Frame
    register :destino
    register :gastar, :comprar
    probe_value -2

    def to_json(*args)
      {
        gastar: self.gastar,
        destino: self.destino
      }.to_json(*args)
    end
  end

  ##
  # Clase que implementa la trama del protocolo C
  class MessageC < Frame
    register :destino
    register :disminuir, :comprar
    probe_value -3

    def to_json(*args)
      {
        disminuir: self.disminuir,
        destino: self.destino
      }.to_json(*args)
    end
  end
end
