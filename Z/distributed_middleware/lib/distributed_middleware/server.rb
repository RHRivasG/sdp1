# frozen_string_literal: true

require_relative 'listener'
require_relative 'message'
require 'socket'
require 'ipaddr'

##
# Clase que implementa el middleware de parte servidor
class Server < MiddlewareListener::Listener
  # Cuando una trama contenga la llave "comprar", deserializar como trama del
  # protocolo Z
  when_key :comprar, parse: Protocol::MessageZ

  def initialize(addr, port)
    super(addr, port)
  end

  ##
  # Sobrescribe el metodo del padre, se vincula a una tienda ingresada por
  # el invocador
  def run
    puts "Introduzca la direccion IPv4 de la tienda a vincular: [host:port]"
    ip, port = gets.chomp.split(/:/)
    puts "Introduzca el protocolo al que la tienda responde: [A|B|C]"

    protocol = gets.chomp

    if protocol == "A"
      klass = Protocol::MessageA
    elsif protocol == "B"
      klass = Protocol::MessageB
    elsif protocol == "C"
      klass = Protocol::MessageC
    end

    add_destination(ip, port.to_i, &klass.method(:of))

    super # Inicia el loop de eventos
  end

  protected

  # def prepare_socket(sock)
  #   membership = IPAddr.new('224.0.0.1').hton + IPAddr.new(@address).hton
  #   sock.setsockopt(:SOL_SOCKET, :SO_REUSEPORT, 1)
  #   sock.setsockopt(:IPPROTO_IP, :IP_ADD_MEMBERSHIP, membership)
  #   super(sock)
  # end
end
