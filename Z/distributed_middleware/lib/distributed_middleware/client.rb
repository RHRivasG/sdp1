# frozen_string_literal: true

require 'json'
require 'yaml'
require 'ipaddr'
require_relative 'listener'
require_relative 'message'
require_relative 'socket'

##
# Clase que implementa el middleware de lado cliente
class Client < MiddlewareListener::Listener
  # Cuando la trama contenga la llave "consumir", interpretar como trama de
  # protocolo A
  when_key :consumir, parse: Protocol::MessageA
  # Cuando la trama contenga la llave "gastar", interpretar como trama de
  # protocolo B
  when_key :gastar, parse: Protocol::MessageB
  # Cuando la trama contenga la llave "disminuir", interpretar como trama de
  # protocolo C
  when_key :disminuir, parse: Protocol::MessageC

  include MiddlewareListener

  def initialize(addr, port, file_dir)
    super(addr, port)
    @destination_config_file = File.join(__dir__, file_dir)
  end

  ##
  # Sobrescribe el metodo del padre, lee las direcciones del archivo de
  # configuracion, las sondea para obtener las direcciones de las tiendas a
  # las que responden
  def prepare_socket(socket)
    super(socket)
    YAML.load_file(@destination_config_file).each do |address|
      puts "Registrando middleware servidor: #{address}"

      host, port = address.split(/:/)

      probe_pkt =
        JSON.generate(Protocol::MessageZ.new(comprar: -1, destino: address))

      socket.send(
        probe_pkt,
        0,
        host,
        port
      )

      pkg, _ = socket.recvfrom_timeout(1024, 10)

      not puts "Tiempo de espera excedido" and next if pkg.nil?

      result = JSON.parse(pkg)
      add_destination(destination = Destination.new(host, port.to_i)) do |dest|
        result.each { |config| dest.register(config["host"], config["port"]) }
      end

      puts "Registrado servidor #{address} con tiendas: #{destination.to_address}"
    end
  end
end
