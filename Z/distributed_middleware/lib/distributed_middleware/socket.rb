# frozen_string_literal: true

require 'socket'

class UDPSocket
  def recvfrom_timeout(max_size, timeout, *args)
    waited = false
    begin
      return if waited

      return recvfrom_nonblock(max_size, *args)
    rescue IO::WaitReadable
      waited = true if IO.select([self], nil, nil, timeout).nil?
      retry
    end
  end
end
