# frozen_string_literal: true

require_relative "distributed_middleware/version"
require_relative "distributed_middleware/client"
require_relative "distributed_middleware/server"
require 'socket'
require 'ipaddr'
require 'optparse'

options = {}
module DistributedMiddleware
  class Error < StandardError; end

  def self.run(args)
    parse = { conf_file: "forwarding.yaml" }
    builder = OptionParser.new do |opts|
      opts.banner =
        "Proceso middleware del primer proyecto de sistemas distribuidos
         version: #{DistributedMiddleware::VERSION}
         Usage: distributed_middleware.rb -s [addr:port]
                distributed_middleware.rb -c [addr:port] -f [configuration file]"

      opts.on(
        "-sENDPOINT",
        "--server=ENDPOINT",
        <<-SERVER.gsub(/\\n/, " ").gsub(/\s+/, " ").strip
        Inicia el middleware de parte servidor vinculado al endpoint entregado,
        ENDPOINT ~= /(\d{3}\.){4}:\\d+/
        SERVER
      ) do |endpoint|
        parse[:type] = "server"
        host, port = endpoint.split(/:/)
        parse[:host], parse[:port] = host, port.to_i != 0 ? port.to_i : 5434
      end

      opts.on(
        "-cENDPOINT",
        "--client=ENDPOINT",
        <<-CLIENT.gsub(/\\n/, " ").gsub(/\s+/, " ").strip
        Inicia el middleware de parte cliente vinculado al endpoint entregado,
        ENDPOINT ~= /(\d{3}\.){4}:\\d+/
        CLIENT
      ) do |endpoint|
        parse[:type] = "client"
        host, port = endpoint.split(/:/)
        parse[:host], parse[:port] = host, port.to_i != 0 ? port.to_i : 5435
      end

      opts.on(
        "-fCONFIGURATION",
        "--f=CONFIGURATION",
        <<-CONFIGURATION.gsub(/\\n/, " ").gsub(/\s+/, " ").strip
        Entrega al middleware cliente el archivo con las direcciones de
        middleware servidor a registrar, debe ser formato YAML con un
        array de ENDPOINT
        CONFIGURATION
      ) do |file|
        parse[:conf_file] = file
      end

      opts.on("-h", "--help", "Muestra este mensaje de ayuda") do
        puts opts
      end
    end

    builder.parse("--help")
    builder.parse!(args)

    yield parse
  end
end

DistributedMiddleware.run(ARGV) do |params|
  puts ""
  if params[:type] == "client"
    Client.new(params[:host], params[:port], params[:conf_file]).run
  elsif params[:type] == "server"
    Server.new(params[:host], params[:port]).run
  end
end
