const { app, ipcMain, BrowserWindow } = require("electron");
const path = require("path");
const udp = require("dgram");

app.on("ready", () => {
  const mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  mainWindow.loadFile(path.join(__dirname, "public/index.html"));
  mainWindow.webContents.openDevTools();
});

//Cliente
var client = udp.createSocket("udp4");

client.on("error", function (error) {
  console.log("Error: " + error);
  client.close();
});

client.on("listening", function () {
  var address = client.address();
  var port = address.port;
  var family = address.family;
  var ipaddr = address.address;
  console.log("Cliente escuchando en el puerto:" + port);
  console.log("Cliente ip :" + ipaddr);
  console.log("Cliente Ip4/Ip6: " + family);
});

client.on("close", function () {
  console.log("Socket is closed");
});

//Creacion de conexion del cliente
//escuchando en el puerto 5555
client.bind(5555, "192.168.1.8");

ipcMain.on("settings", (e, arg) => {
  json = JSON.stringify({
    disminuir: -3,
    destino: null,
  });

  console.log(arg);

  //Enviar peticion de configuracion al host y puertos
  //asociados en el cliente, para efectos de devolver
  //todas las tiendas que estan escuchando
  client.send(json, arg.port, arg.host, function (error) {
    if (error) {
      client.close();
    } else {
      console.log("Configurando");
    }
  });

  msgfun = function (msg, info) {
    console.log(
      "Recibido %d bytes desde %s:%d\n",
      msg.length,
      info.address,
      info.port
    );

    //Recibe respuesta y la retorna a la pantalla
    e.reply("reply", msg.toString());
    client.removeListener("message", msgfun);
  };

  client.on("message", msgfun);
});

ipcMain.on("msg", (e, arg) => {
  json = JSON.stringify({
    disminuir: arg.nrop,
    destino: arg.store,
  });

  //Enviar peticion de productos al host y puertos
  //asociados en el cliente
  client.send(json, arg.port, arg.host, function (error) {
    if (error) {
      client.close();
    } else {
      console.log("Pedido enviado");
      console.log(Date(Date.now()));
      console.log("Consumidor :5555");
      console.log("Cantidad: " + arg.nrop);
      console.log("Destino: " + arg.store);
    }
  });

  msgfun = function (msg, info) {
    console.log(
      "Recibido %d bytes desde %s:%d\n",
      msg.length,
      info.address,
      info.port
    );

    //Recibe respuesta y la retorna a la pantalla
    e.reply("reply", msg.toString());
    client.removeAllListeners("message");
  };

  client.on("message", msgfun);
});
